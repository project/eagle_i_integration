<?php

/**
* @param $form
* @return array
*/
function eagle_i_integration_test_data_mapper($form)
{
  $form = array();
  $form['NID'] = array(
    '#type' => 'textfield',
    '#title' => t('NID'),
    '#default value' => '',
  );
  $form['map_node'] = array(
    '#type' => 'submit',
    '#value' => 'Map Node',
    '#ahah' => array(
      'path' => 'eagle-i/test/data_mapper/map_node/callback',
      'wrapper' => 'test-results',
    ),
  );
  $form['test-results'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="test-results">',
    '#suffix' => '</div>',
    '#value' => '&nbsp;',
  );

  // Testing and Config links.
  $form['map_data_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/map-data">Map Data to Eagle-I</a>',
    '#prefix' => '<div id="eagle-i-testing-links">',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['connector_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i">Eagle-I Configuration Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['mapper_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/eagle-i/test/data_connector">DataConnector Test Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['uri_map_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/uri-map">Manually Map Node IDs to URIs</a>',
    '#suffix' => '</div>',
  );
  return $form;
}

function eagle_i_integration_test_data_mapper_map_node_callback()
{
  $NID = eagle_i_integration_test_data_mapper_fetch_NID();

  if (!$NID)
  {
    $output = 'Please enter a node id.';
  }
  else
  {
    try
    {
      $controller = EIController::getInstance();
      $output = $controller->testDataMapper($NID);
    }
    catch (Exception $ex)
    {
      $output = $ex->getMessage();
    }
  }

  $output = '
      <div id="test-results" style="border: 5px solid #eee; margin: 20px 0; padding: 0 20px; word-wrap: break-word;">
      <pre>' . $output . '</pre>
      </div>
    ';

  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

function eagle_i_integration_test_data_mapper_fetch_NID()
{
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $URI = $form['NID']['#value'];
  drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  return $URI;
}