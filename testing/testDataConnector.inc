<?php

/**
 * Implementation of hook_form
 * @param $form
 * @return array
 */
function eagle_i_integration_test_data_connector($form)
{
  if (variable_get('api_server_url', '') == '')
  {
    drupal_set_message('You have not designated an API server. Please visit the Eagle-I Configuration Page at /admin/config/eagle-i and designate a server before attempting to test your API connection.', 'error');
  }

  $form = array();
  $form['URI'] = array(
    '#type' => 'textfield',
    '#title' => t('URI'),
    '#default value' => '',
  );
  $form['get_resource'] = array(
    '#type' => 'submit',
    '#value' => 'Get Resource',
    '#ahah' => array(
      'path' => 'eagle-i/test/data_connector/resource/callback',
      'wrapper' => 'test-results',
    ),
  );
  $form['create_or_update_resource'] = array(
    '#type' => 'submit',
    '#value' => 'Create New Resource from Given URI',
    '#ahah' => array(
      'path' => 'eagle-i/test/data_connector/update/resource/callback',
      'wrapper' => 'test-results',
    ),
    '#suffix' => '<br />',
  );
  /*$form['get_ontology_class'] = array(
    '#type' => 'submit',
    '#value' => 'Get Ontology Class',
    '#ahah' => array(
      'path' => 'eagle-i/test/data_connector/class/callback',
      'wrapper' => 'test-results',
    )
  );*/
  $form['get_model'] = array(
    '#type' => 'submit',
    '#value' => t('Get Ontology Model'),
    '#ahah' => array(
      'path' => 'eagle-i/test/data_connector/model/callback',
      'wrapper' => 'test-results',
    ),
  );
  $form['test-results'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="test-results">',
    '#suffix' => '</div>',
    '#value' => '&nbsp;',
  );

  // Testing and Config links.
  $form['map_data_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/map-data">Map Data to Eagle-I</a>',
    '#prefix' => '<div id="eagle-i-testing-links">',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['connector_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i">Eagle-I Configuration Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['mapper_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/eagle-i/test/data_mapper">DataMapper Test Page</a>',
    '#suffix' => '</div>',
  );
  $form['uri_map_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/uri-map">Manually Map Node IDs to URIs</a>',
  );
  return $form;
}

/**
 * AHAH callback for the get resource button.
 */
function eagle_i_integration_test_data_connector_resource_callback()
{
  $URI = eagle_i_integration_test_data_connector_fetch_URI();

  if (!$URI)
  {
    $output = 'Please enter a URI.';
  }
  else
  {
    $controller = EIController::getInstance();
    $output = $controller->testDataConnector('resource', $URI);
  }
  $output = '
    <div id="test-results" style="border: 5px solid #eee; margin: 20px 0; padding: 0 20px; word-wrap: break-word;">
    <pre>' . $output . '</pre>
    </div>
  ';

  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

/**
 * AHAH callback for the update resource button.
 */
function eagle_i_integration_test_data_connector_update_resource_callback()
{
  $URI = eagle_i_integration_test_data_connector_fetch_URI();

  if (!$URI)
  {
    $output = 'Please enter a URI.';
  }
  else
  {
    $controller = EIController::getInstance();
    $output = $controller->testDataConnector('update', $URI);
  }
  $output = '
    <div id="test-results" style="border: 5px solid #eee; margin: 20px 0; padding: 0 20px; word-wrap: break-word;">
    <pre>' . $output . '</pre>
    </div>
  ';

  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

// TODO: THIS IS NOT UTILIZED IN THE CURRENT RELEASE.
/**
 * AHAH callback for the get ontology class button.
 */
function eagle_i_integration_test_data_connector_class_callback()
{
  $URI = eagle_i_integration_test_data_connector_fetch_URI();

  if (!$URI)
  {
    $output = 'Please enter a URI.';
  }
  else
  {
    $controller = EIController::getInstance();
    $output = $controller->testDataConnector('class', $URI);
  }
  $output = '
    <div id="test-results" style="border: 5px solid #eee; margin: 20px 0; padding: 0 20px; word-wrap: break-word;">
    <pre>' . $output . '</pre>
    </div>
  ';

  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

/**
 * AHAH callback for the get model version button.
 */
function eagle_i_integration_test_data_connector_model_callback()
{
  $controller = EIController::getInstance();
  $output = $controller->testDataConnector('model');
  $output = '
    <div id="test-results" style="border: 5px solid #eee; margin: 20px 0; padding: 0 20px; word-wrap: break-word;">
    <pre>' . $output . '</pre>
    </div>
  ';

  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

/**
 * Helper function to retrieve the URI from the form.
 * @return mixed
 */
function eagle_i_integration_test_data_connector_fetch_URI()
{
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $URI = $form['URI']['#value'];
  drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  return $URI;
}