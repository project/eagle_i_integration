<?php

/**
 * Implementation of hook_schema.
 * @return array
 */
function eagle_i_integration_schema() {
  return array(
    'eagle_i_activity' => array(
      'description' => t('Eagle-I Integration module activity log.'),
      'fields' => array(
        'aid' => array(
          'description' => t('The primary key for logged activity'),
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'nid' => array(
          'description' => t('The node id of the node being passed to the module.'),
          'type' => 'int',
          'not null' => TRUE,
        ),
        'eid' => array(
          'description' => t('The error id, if any, for the current activity.'),
          'type' => 'int',
          'not null' => FALSE,
        ),
        'timestamp' => array(
          'description' => t('The php timestamp of the current activity.'),
          'type' => 'int',
          'not null' => TRUE,
        ),
        'EIExchangeInstance' => array(
          'description' => t('The serialized EIExchangeInstance of the node created by the EIDataMapper.'),
          'type' => 'text',
        ),
        'EIInstanceUpdateRequest' => array(
          'description' => t('The serialized EIInstanceUpdateRequest returned by the EIDataConnector.'),
          'type' => 'text',
        ),
      ),
      'primary key' => array('aid'),
    ),
    'eagle_i_errors' => array(
      'description' => t('Eagle-I Integration module errors log.'),
      'fields' => array(
        'eid' => array(
          'description' => t('The primary key for logged errors.'),
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'ErrorMessage' => array(
          'description' => t('The error message returned by the thrown error.'),
          'type' => 'text',
        ),
      ),
      'primary key' => array('eid'),
    ),
    'eagle_i_uris' => array(
      'description' => t('Eagle-I Integration module Drupal node IDs mapped to Eagle-I URIs.'),
      'fields' => array(
        'nid' => array(
          'description' => t('The Drupal node ID for the entity.'),
          'type' => 'int',
          'not null' => TRUE,
        ),
        'uri' => array(
          'description' => t('The Eagle-I URI for the entity.'),
          'type' => 'text',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('nid'),
    ),
  );
}

/**
 * Implementation of hook_install.
 */
function eagle_i_integration_install() {
  // Install the db scheme and enforce cascading of eids.
  drupal_install_schema('eagle_i_integration');
  db_query("
    ALTER TABLE eagle_i_activity
    ADD CONSTRAINT eid
    FOREIGN KEY (eid)
    REFERENCES eagle_i_errors (eid)
    ON DELETE SET NULL;
  ");

  // Input any uris from a previous install into the db.
  // TODO: Getting an error here when the file is not present.
  $path = drupal_get_path('module', 'eagle_i_integration');
  $fp = fopen($path . "/uri_map.csv", "r");
  if ($fp) {
    while (!feof($fp)) {
      $nl = fgets($fp, 4096);
      $arr = explode(',', $nl);
      $arr[1] = substr($arr[1], 0, -1);
      db_query("
        INSERT INTO eagle_i_uris (nid, uri) VALUES ('%s', '%s');
      ", $arr);
    }
    fclose($fp);
  }
}

/**
 * Implementation of hook_uninstall.
 */
function eagle_i_integration_uninstall() {
  // TODO: Getting an error here occasionally.
  // Dump the eagle_i_uris table to a .csv file for persistent
  // node id to uri mapping between installations.
  $path = drupal_get_path('module', 'eagle_i_integration');
  $fp = fopen($path . "/uri_map.csv", "w");
  $result = db_query("SELECT * FROM eagle_i_uris");
  while ($row = db_fetch_array($result)) {
    fwrite($fp, $row['nid'] . ',' . $row['uri'] . "\n");
  }
  fclose($fp);

  // Uninstall the db schema.
  drupal_uninstall_schema('eagle_i_integration');

  // Remove the eaglei_export field from mapped content types.
  module_load_include('inc', 'content', 'includes/content.crud');
  $content_types = unserialize(variable_get('mapped_content_types', null));
  if ($content_types) {
    foreach ($content_types as $content_type) {
      content_field_instance_delete('field_eaglei_export', $content_type, FALSE);
    }
  }
  content_clear_type_cache(TRUE);
  menu_rebuild();

  // Delete the Drupal variables.
  $vars = array(
    'eagle_i_source_name',
    'eagle_i_enable_node_save',
    'mapped_content_types',
    'eagle_i_batch_size',
    'ontology_version',
    'ei_queue',
    'api_server_url',
    'api_rest_url',
    'api_username',
    'api_password',
  );
  foreach ($vars as $var) {
    variable_del($var);
  }
}