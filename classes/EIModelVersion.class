<?php

/**
 * The EIModelVersion object encapsulates an ontology model/version request response.
 * This class is immutable, and can only be constructed from received JSON data
 * from the Eagle-I API.
 */
class EIModelVersion extends EIContainer {
  /**#@+
   * @access private
   * @var string
   */
  private $loaded;
  private $availableJar;
  private $availableModel;
  /**#@-*/

  public function __construct($xml_data) {
    if ($xml_data) {
      $this->set($xml_data);
    }
  }

  /**
   * Get the version number for the currently loaded ontology (the loaded property).
   * @return string
   */
  public function getVersion() {
    return $this->loaded;
  }

  /**
   * Returns true if the object is null, otherwise returns false.
   * @return bool
   */
  public function is_null() {
    return is_null($this->loaded) && is_null($this->availableJar) && is_null($this->availableModel);
  }

  /**
   * Returns a serialized JSON representation of the object.
   * @param bool $pretty True if output with indentation is desired.
   * @return string
   */
  public function json_encode($pretty = FALSE) {
    if ($pretty) {
      return "{\n\tloaded:\"" . $this->json_escape($this->loaded) . "\",\n\tavailableJar:\"" . $this->json_escape($this->availableJar) . "\",\n\tavailableModel:\"" . $this->json_escape($this->availableModel) . "\"\n}";
    }
    return '{{loaded:"' . $this->json_escape($this->loaded) . '"},{availableJar:"' . $this->json_escape($this->availableJar) . '"},{availableModel:"' . $this->json_escape($this->availableModel) . '"}}';
  }

  /**
   * Sets the properties as per the passed JSON data.
   * @access private
   * @param $data
   * @throws Exception If the JSON data cannot be used to populate the object.
   */
  private function set($data) {
    $data = simplexml_load_string($data);
    foreach ($data->results->result->binding as $element) {
      $key = (string) $element->attributes()->name;
      $this->$key = (string) $element->literal;
    }
  }
}
