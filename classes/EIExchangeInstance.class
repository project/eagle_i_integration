<?php

/**
 * The EIExchangeInstace encapsultes an Eagle-I Exchange Instance object.
 * This is the main object passed between the API when querying and updating
 * ontology class instances in the Eagle-I ontology.
 */
class EIExchangeInstance extends EIContainer {
  /**
   * @access private
   * @var EIEntity
   */
  private $instanceEntity;

  /**
   * @access private
   * @var EIURI
   */
  private $specificTypeUri;

  /**
   * @access private
   * @var EIURI
   */
  private $rootType;

  /**
   * @access private
   * @var string
   */
  private $foreignKey;

  /**
   * @access private
   * @var null
   */
  private $source;

  /**
   * @access private
   * @var array(<string> => EIEntity)
   */
  private $linkedResources;

  /**
   * @access private
   * @var array(<string> => <string>)
   */
  private $textProperties;

  public function __construct($json_data = FALSE) {
    $this->specificTypeUri = null;
    $this->rootType = null;
    $this->foreignKey = null;
    $this->source = null;
    $this->linkedResources = array();
    $this->textProperties = array();

    if ($json_data) {
      $this->set($json_data);
    }
  }

  /**
   * Sets all URIs in the object to null.
   * This is only used for testing, when returning a EIExchangeInstance back
   * to the Eagle-I node after just retrieving it in order to create a new instance.
   * @throws Exception if the instanceEntity property is null (if the object
   * was instantiated as a null object).
   */
  public function clearURIs() {
    if (is_null($this->instanceEntity))
      throw new Exception ('Null entity');

    $this->instanceEntity->setURI(new EIURI());
    foreach ($this->linkedResources as $v) {
      $v->instanceEntity->setURI(new EIURI());
    }
    $this->foreignKey = 1;
    $this->source = 'Fred Hutchinson Cancer Research Center';
  }

  /**
   * Populates the object properties from a passed JSON string.
   * @param $data The JSON string.
   * @throws Exception If the JSON string is incomplete or malformed.
   * @access private
   */
  private function set($data) {
    if (!isset($data->instanceEntity) || !isset($data->specificTypeUri) || !isset($data->rootType)
      || !isset($data->foreignKey)  || !isset($data->source) || !isset($data->linkedResources)
      || !isset($data->textProperties))
      throw new Exception('Data mismatch error');

    $this->instanceEntity = new EIEntity($data->instanceEntity);
    $this->specificTypeUri = new EIURI($data->specificTypeUri);
    $this->rootType = new EIURI($data->rootType);
    $this->foreignKey = $data->foreignKey;
    $this->source = $data->source;
    foreach ($data->linkedResources as $key => $value) {
      $resource = new EIExchangeInstance($value[0]);
      $this->pushLinkedResource(array($key, $resource));
    }
    foreach ($data->textProperties as $key => $value) {
      $this->pushTextProperties(array($key, $value[0]));
    }
  }

  /**
   * Get the instanceEntity property.
   * @return EIEntity
   */
  public function getInstanceEntity() {
    return $this->instanceEntity;
  }

  /**
   * Set the instanceEntity property.
   * @param EIEntity $entity
   */
  public function setInstanceEntity(EIEntity $entity) {
    $this->instanceEntity = $entity;
  }

  /**
   * Get the specificTypeUri property.
   * @return EIURI|null
   */
  public function getSpecificTypeUri() {
    return $this->specificTypeUri;
  }

  /**
   * Set the specificTypeUri property.
   * @param EIURI $eiuri
   */
  public function setSpecificTypeUri(EIURI $eiuri) {
    $this->specificTypeUri = $eiuri;
  }

  /**
   * Get the rootType property.
   * @return EIURI|null
   */
  public function getRootType() {
    return $this->rootType;
  }

  /**
   * Set the rootType property.
   * @param EIURI $eiuri
   */
  public function setRootType(EIURI $eiuri) {
    $this->rootType = $eiuri;
  }

  /**
   * Get the foreignKey property.
   * @return null|string
   */
  public function getForeignKey() {
    return $this->foreignKey;
  }

  /**
   * Set the foreignKey property.
   * @param $key
   */
  public function setForeignKey($key) {
    $this->foreignKey = $key;
  }

  /**
   * Get the source property.
   * @return null|string
   */
  public function getSource() {
    return $this->source;
  }

  /**
   * Set the source property.
   * @param $source
   */
  public function setSource($source) {
    $this->source = $source;
  }

  /**
   * Get the linkedResources property.
   * @return array<EIExchangeInstance>
   */
  public function getLinkedResources() {
    return $this->linkedResources;
  }

  /**
   * Set the linkedResources property.
   * @param array $resources
   */
  public function setLinkedResources(array $resources) {
    $this->linkedResources = $resources;
  }

  /**
   * Pop the top linkedResource from the linkedResources property.
   * Note: This will remove the resource from the object
   * @return EIExchangeEntity
   */
  public function popLinkedResource() {
    if (count($this->linkedResources > 0)) {
      return array_pop($this->linkedResources);
    }
    return null;
  }

  /**
   * Push a resources to the linkedResources property.
   * @param array(<string:uri> => <EIExchangeInstance>) $resource
   */
  public function pushLinkedResource(array $resource) {
    $this->linkedResources[$resource[0]] = $resource[1];
  }

  /**
   * Get the textProperties property.
   * @return array(<string:uri> => <string:property>)
   */
  public function getTextProperties() {
    return $this->textProperties;
  }

  /**
   * Set the textProperties property.
   * @param array(<string:uri> => <string:property>) $properties
   */
  public function setTextProperties(array $properties) {
    $this->textProperties = $properties;
  }

  /**
   * Pop the top textProperty from the textProperties property.
   * Note: This will remove the property from the object
   * @return array(<string:uri> => <string:property>)
   */
  public function popTextProperties() {
    if (count($this->textProperties > 0)) {
      return array_pop($this->textProperties);
    }
    return null;
  }

  /**
   * Push a textProperty to the textProperties property.
   * @param array(<string:uri> => <string:property>) $resource
   */
  public function pushTextProperties(array $resource) {
    $this->textProperties[$resource[0]] = $resource[1];
  }

  /**
   * Returns true if the object is null, otherwise returns false.
   * @return bool
   */
  public function is_null() {
    return is_null($this->instanceEntity) && is_null($this->specificTypeUri)
      && is_null($this->rootType) && is_null($this->foreignKey) && is_null($this->source);
  }

  /**
   * Returns a serialized JSON representation of the object.
   * @param bool $pretty True if output with indentation is desired.
   * @return string
   */
  public function json_encode($pretty = FALSE) {
    if (is_null($this->instanceEntity) || is_null($this->specificTypeUri) || is_null($this->rootType))
      throw new Exception ('Null entity');

    $out = ($pretty) ? "{\n" : '{';

    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "\"instanceEntity\""
      . $this->instanceEntity->json_encode($pretty + 1) . ",\n"
      : '"instanceEntity":' . $this->instanceEntity->json_encode() . ',';
    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "\"specificTypeUri\":"
      . $this->specificTypeUri->json_encode($pretty + 1) . ",\n"
      : '"specificTypeUri":' . $this->specificTypeUri->json_encode() . ',';
    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "\"rootType\":"
      . $this->rootType->json_encode($pretty + 1) . ",\n"
      : '"rootType":' . $this->rootType->json_encode() . ',';
    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "\"foreignKey\":"
      . $this->json_escape($this->foreignKey) . ",\n" : '"foreignKey":"' . $this->json_escape($this->foreignKey) . '",';
    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "\"source\":"
      . $this->json_escape($this->source) . ",\n" : '"source":"' . $this->json_escape($this->source) . '",';
    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "\"linkedResources\":{\n"
      : '"linkedResources":{';

    foreach ($this->linkedResources as $k => $v) {
      $out .= ($pretty) ? $this->json_indent($pretty + 2) . "\"" . $this->json_escape($k) . "\":["
        . $v->json_encode($pretty + 2) . "],\n"
        : '"' . $this->json_escape($k) . '":[' . $v->json_encode() . '],';
    }
    if (count($this->linkedResources) > 0) {
      $out = ($pretty) ? substr($out, 0, -2) . "\n" : substr($out, 0, -1);
    }

    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "},\n" : '},';
    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "\"textProperties\":{\n" : '"textProperties":{';
    foreach ($this->textProperties as $k => $v) {
      $out .= ($pretty) ? $this->json_indent($pretty + 2)
        . "\"" . $this->json_escape($k) . "\":[\"" . $this->json_escape($v) . "\"],\n"
        : '"' . $this->json_escape($k) . '":["' . $this->json_escape($v) . '"],';
    }
    if (count($this->textProperties) > 0) {
      $out = ($pretty) ? substr($out, 0, -2) . "\n" : substr($out, 0, -1);
    }

    $out .= ($pretty) ? $this->json_indent($pretty + 1) . "}\n" : '}';

    $out .= ($pretty) ? $this->json_indent($pretty) . "}" : '}';

    return $out;
  }
}