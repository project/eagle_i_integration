Eagle-I Integration module for
Fred Hutchinson Cancer Research Center



Current Implementation

The module currently supports mapping specified Drupal nodes to Eagle-I Ontology Class Instances
and uploading of these Instances to an Eagle-I node for inclusion in the ontology.

There are five main administration pages that should appear on your administration menu once the
module is loaded (you may need to clear the cache to rebuild the menu router table).  The pages
are as follows:

/admin/config/eagle-i		This is the main configuration page
/admin/config/eagle-i/map-data	 This is the main UI for data mapping
/admin/config/eagle-i/uri-map   This allows the admin to manually map nids to uris
/eagle-i/test/data_mapper		A testing page for the data mapping functionality
/eagle-i/test/data_connector	A testing page for the RESTful API connection

You will first need to define the API settings and credentials on the main configuration page. Once
your Eagle-I node is set up, enter the requested information and click save.

Once your server is set up, you'll want to go to the map-data page (you'll find a link at the bottom
of the main configuration page).  Once there, you'll first want to update the ontology version.  If
you are unable to do this, your API connection is not set up properly.  Once updated you can then use
the UI to select the setting you wish to use for transferring and mapping nodes.  There are two things
to note about this:

1. The map data is contained in an xml file: xml/content_type_mapping.xml in the module directory.
You will need to enter mapping data (Drupal fields to Ontology Class properties) directly into the
.xml file.  Refer to the xml schema (xml/content_type_mapping.xsd) for necessary metadata.

2. When you select to include a content type in the content type mapping, this will automatically add
the field eaglei_export to the content type, and set it to true for all nodes of that type.

You may then test the your mappings and your data connection with the respective pages. When testing
the data connector, note that the Create New Resource from Given URI will do just that – if will create
a new copy of the object at the given URI on the Eagle-I node with a new URI.  You'll then need to go
into the Sweet and remove the object if you don't want it there.

Once the module is enabled and configured, and all content types selected are mapped in the .xml document,
you may select “enabled” under “Push to Eagle-I on Node Save” on the data mapping page, and any node of a
selected content type that has the eaglei_export field set to true will be pushed to Eagle-I every time the
node is saved.


IMPORTANT NOTES - new for v.6.x-1.3

1) Node id to Eagle-I uri mapping is now persisitent between installations in v6.x-1.3. This map is contained
in the db table 'eagle_i_uris', and will be dumped to a .txt file in the main module directory on uninstall.
The install hook will check to see if this file exists first, creating and populating the table from the
file if it does. You map port this module to new versions of your site, or new instances, simply uninstall
the module, and place the .txt file in the new site module base directory prior to installation. In this way
you will not create duplicate instances in the Eagle-I ontology.

2) Node id to Eagle-I uri mapping can be accomplished manually via the admin/config/eagle-i/map-uri page.

3) API authentication credentials are stored as securly as possible. At the time of development, the Eagle-I
ontology API provides a simple HTTP authentication only.



Opportunities for Future Improvement

1. Allow data mapping in the UI as opposed to the .xml document



Documentation

Php documentation is available in the /docs folder of the module path; navigate to the path in a browser to
view the documentation.  A class diagram is also available in the /docs folder titled class_diagram.pdf.