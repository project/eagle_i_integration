$(document).ready(function() {
  $("table#uri-mappings td input[type=submit]").not(".new").click(function() {
    if ($(this).val() == 'x') {
      if (!confirm("Are you sure you want to delete this URI mapping from the database?")) {
        return false;
      }
    }
    return true;
  });
});