<?php

function eagle_i_integration_batch_method_process_node($nid, $bool = FALSE) {
  $controller = EIController::getInstance();
  $controller->processNode($nid, $bool);
}