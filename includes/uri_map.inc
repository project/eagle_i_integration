<?php

function eagle_i_integration_uri_map_form(&$form_state) {
  drupal_add_js(drupal_get_path('module', 'eagle_i_integration') . '/scripts/uri_map.js');

  $form = array();
  $form['header'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="uri-map-header">',
    '#suffix' => '</div>',
    '#value' => t('<p>All nodes that have been mapped to Eagle-I are listed here. If a node is not listed here, a new
      instance will be created on Eagle-I when that node is pushed. If you have a node that is not pushed, but you
      <em>already have an instance on Eagle-I</em>, you may enter the node id and the Eagle-I URI here.</p><p>It is
      important to note that these mappings will persist between Eagle-I installations. If you want to remove this map
      and start fresh (<em>all</em> pushes to Eagle-I will then create new instances), disable and uninstall the module,
      the remove the file uri_map.csv from the base module directory, and re-enable the module.</p><p>Additionally,
      if you would like to move your Drupal installation, but keep your URI map (preventing you from creating all new
      copies of your nodes on Eagle-I) simply disable and uninstall the module, move the uri_map.csv file created by the
      uninstall process to the base directory of this module on the new installation, and enable the module.</p>'),
  );
  $form['uri_map'] = array(
    '#type' => 'markup',
    '#tree' => TRUE,
    '#prefix' => '<table id="uri-mappings">'
      . '<thead><tr><th>Node ID</th><th>Node Title</th><th>URI</th><th></th></tr></thead>',
    '#suffix' => '</table>',
  );

  $result = db_query("
    SELECT * FROM eagle_i_uris ORDER BY nid;
  ");
  while ($row = db_fetch_array($result)) {
    $q = db_query("
      SELECT title FROM node WHERE nid = %s
    ", $row['nid']);
    $r = db_fetch_array($q);
    $title = ($r) ? $r['title'] : '';
    $form['uri_map'][$row['nid']] = array(
      '#type' => 'markup',
      '#tree' => TRUE,
      '#prefix' => '<tr><td>' . $row['nid'] . '</td><td>' . $title . '</td><td>',
      '#suffix' => '</td></tr>',
    );
    $form['uri_map'][$row['nid']][$row['nid'] . '_uri'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => $row['uri'],
    );
    $form['uri_map'][$row['nid']]['remove'] = array(
      '#type' => 'submit',
      '#value' => 'x',
      '#name' => $row['nid'],
      '#attributes' => array('class' => 'remove_uri_mapping'),
      '#prefix' => '</td><td>',
      '#suffix' => '</td>',
      '#submit' => array('eagle_i_integration_remove_uri_map_callback'),
    );
  }

  if (isset($form_state['storage']['new_mapping'])) {
    $form['uri_map']['new_mapping'] = array(
      '#type' => 'markup',
      '#tree' => TRUE,
      '#prefix' => '<tr>',
      '#suffix' => '</tr>',
    );
    $form['uri_map']['new_mapping']['new_nid'] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#default_value' => '',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#ahah' => array(
        'path' => 'admin/config/eagle-i/uri-map/add-mapping',
        'wrapper' => 'new-node-title',
        'event' => 'blur',
      ),
    );
    $form['uri_map']['new_mapping']['new_node_title'] = array(
      '#type' => 'markup',
      '#prefix' => '<td id="new-node-title">',
      '#suffix' => '</td>',
      '#value' => '&nbsp;',
    );
    $form['uri_map']['new_mapping']['new_uri'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#default_value' => '',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
    $form['uri_map']['new_mapping']['remove'] = array(
      '#type' => 'submit',
      '#value' => 'x',
      '#name' => 'new',
      '#attributes' => array('class' => 'remove_uri_mapping new'),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#submit' => array('eagle_i_integration_remove_uri_map_callback'),
    );
  }

  $form['uri_map']['footer'] = array(
    '#type' => 'markup',
    '#prefix' => '<tfoot>',
    '#suffix' => '</tfoot>',
  );

  if (empty($form_state['storage']['new_mapping'])) {
    $form['uri_map']['footer']['add'] = array(
      '#type' => 'submit',
      '#value' => '+',
      '#validate' => array('eagle_i_integration_add_uri_mapping'),
      '#prefix' => '<tr><td></td><td></td><td></td><td>',
      '#suffix' => '</td></tr>',
    );
  }
  $form['uri_map']['footer']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    '#submit' => array('eagle_i_integration_save_uri_map'),
    '#prefix' => '<tr><td></td><td></td><td></td><td>',
    '#suffix' => '</td></tr>',
  );

  // Testing and Config links.
  $form['eagle_i_config_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i">Eagle-I Configuration Page</a>',
    '#prefix' => '<div id="eagle-i-testing-links">',
    '#suffix' => '&nbsp;|&nbsp;',
  );$form['map_data_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/map-data">Map Data to Eagle-I</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['connector_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i">Eagle-I Configuration Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['mapper_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/eagle-i/test/data_connector">DataConnector Test Page</a>',
    '#suffix' => '</div>',
  );
  return $form;
}

function eagle_i_integration_save_uri_map($form, &$form_state) {
  unset($form_state['storage']);
  $uri_map = $form_state['values']['uri_map'];
  $new_mapping = (isset($uri_map['new_mapping'])) ? $uri_map['new_mapping'] : FALSE;
  if (isset($uri_map['new_mapping'])) unset($uri_map['new_mapping']);
  unset($uri_map['footer']);
  foreach ($uri_map as $nid => $array) {
    db_query("
      UPDATE eagle_i_uris
      SET uri = '%s'
      WHERE nid = '%s'
    ", array($array[$nid . '_uri'], $nid));
  }
  if ($new_mapping && !empty($new_mapping['new_uri']) && !empty($new_mapping['new_nid'])) {
    $result = @db_query("
      INSERT INTO eagle_i_uris (nid, uri)
      VALUES ('%s', '%s')
    ", array($new_mapping['new_nid'], $new_mapping['new_uri']));
    if (!$result) {
      drupal_set_message('That mapping could not be added. Perhaps there is a mapping for that node already?', 'error');
    }
  }
}

function eagle_i_integration_remove_uri_map_callback($form, &$form_state) {
  $nid = $form_state['clicked_button']['#name'];
  if ($nid == 'new') {
    unset($form['uri_map']['new_mapping']);
    unset($form_state['storage']);
    $form_state['rebuild'] = TRUE;
    return;
  }
  db_query("
    DELETE FROM eagle_i_uris
    WHERE nid = '%s'
  ", $nid);
  unset($form['uri_map'][$nid]);
}

function eagle_i_integration_add_uri_mapping($form, &$form_state) {
  $form_state['storage']['new_mapping'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function eagle_i_integration_add_uri_mapping_callback() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  $nid = (isset($form['#parameters'][1]['post']['uri_map']['new_mapping']['new_nid'])) ?
    $form['#parameters'][1]['post']['uri_map']['new_mapping']['new_nid'] : FALSE;
  $output = '';
  if ($nid) {
    $result = db_query("
      SELECT title FROM node WHERE nid = %s
    ", $nid);
    $row = db_fetch_array($result);
    $output = ($row['title']) ? '<span>' . $row['title'] . '</span>' : '<span>Node not found.</span>';
  }

  drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}