<?php

/**
 * Implementation hook_form for the Data Mapping form.
 * @param $form
 * @return array
 */
function eagle_i_integration_map_data_form($form) {
  $form = array();
  $controller = EIController::getInstance();

  // Get Site Ontology & Node Ontology.
  $ontology_version = variable_get('ontology_version', '');
  $EI_ontology_version = null;
  try {
    $EI_ontology_version = $controller->getOntologyVersion();
  }
  catch (Exception $ex) {
    drupal_set_message("Please set up the API connection of the /admin/config/eagle-i page before attempting to change settings on this page.", 'error');
  }

  // Get all site Content Type, mapped content types, and batch size.
  $source_name = variable_get('eagle_i_source_name', '');
  $content_types = node_get_types('names');
  $mapped_content_types = unserialize(variable_get('mapped_content_types', null));
  $batch_size = variable_get('eagle_i_batch_size', 0);

  // ERRORS
  // Display any pending errors on the form.
  $errors = eagle_i_integration_get_current_errors();
  if (count($errors) > 0) {
    $form['errors'] = array(
      '#type' => 'fieldset',
      '#title' => t('Eagle-I Export Errors'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    foreach ($errors as $nid => $error) {
      $error_html = 'Node #<a href="/' . $error['path'] . '">' . $nid . '</a>: ' . $error['title'] . '<br /><strong><em>' . $error['message'] . '</em></strong>';
      $form['errors']['error-' . $nid] = array(
        '#type' => 'markup',
        '#value' => $error_html,
        '#prefix' => '<div class="eagle-i-error">',
        '#suffix' => '</div>',
      );
    }
  }

  // ONTOLOGY VERSION
  // Display the current Site vs Node ontology - if different,
  // display the update site ontology button.
  $form['ontology_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ontology Version'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ontology_fieldset']['ontology_version'] = array(
    '#type' => 'markup',
    '#value' => 'You are using ontology version: <em>' . $ontology_version . '</em>',
    '#prefix' => '<div class="ontology-wrapper">',
    '#suffix' => '</div>',
  );
  $form['ontology_fieldset']['EI_ontology_version'] = array(
    '#type' => 'markup',
    '#value' => 'Your Eagle-I node is using ontology version: <em>' . $EI_ontology_version . '</em>',
    '#prefix' => '<div class="ontology-wrapper">',
    '#suffix' => '</div>',
  );
  if ($ontology_version !== $EI_ontology_version) {
    $form['ontology_fieldset']['ontology_reset'] = array(
      '#type' => 'submit',
      '#value' => t('Update Site Ontology Version'),
      '#submit' => array('eagle_i_integration_reset_site_ontology_callback'),
      '#prefix' => '<div class="ontology-reset-button-wrapper">',
      '#suffix' => '</div>',
    );
    $form['ontology_fieldset']['ontology_reset_message'] = array(
      '#type' => 'markup',
      '#value' => t('Click this only if you have ensured that all your field mappings are up to date.'),
      '#prefix' => '<div class="ontology-reset-message-wrapper"><span class="arrow">&larr;</span>',
      '#suffix' => '</div>',
    );
  }

  // SOURCE
  // Display the text input for the source variable.
  $form['source_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Source Name'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['source_fieldset']['source_name'] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#description' => t('Enter the name of the source for your Eagle-I objects (usually the name of your organization).'),
    '#size' => 45,
    '#default_value' => $source_name,
  );

  // ENABLE NODE SAVE
  // Display the radio group to enable pushing nodes to Eagle-I on node_save.
  $form['enable_node_save_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Push to Eagle-I on Node Save'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['enable_node_save_fieldset']['enable_node_save_radio'] = array(
    '#type' => 'radios',
    '#title' => t(''),
    '#default_value' => variable_get('eagle_i_enable_node_save', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );

  // CONTENT TYPES
  // List the site content types allowing the user to select which will be mapped.
  $form['content_types_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mapped Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['content_types_fieldset']['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t(''),
    '#description' => t('Select the content types you wish to map to
        corresponding ontology classes in the Eagle-I ontology.'),
    '#options' => $content_types,
    '#default_value' => ($mapped_content_types) ? $mapped_content_types : array(),
    '#prefix' => '<div id="content_type_checkboxes">',
    '#suffix' => '</div>',
  );
  $form['content_types_fieldset']['ct_submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Changes',
  );

  // CONTENT TYPE MAPPING
  //
  _eagle_i_integration_create_type_maps($form, $mapped_content_types);

  // BATCH SETTINGS
  // Set batch size, queue all nodes, clear queue, process batch.
  $form['batch_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Batch Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="batch_settings">',
    '#suffix' => '</div>',
  );
  $form['batch_fieldset']['batch_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch size'),
    '#description' => t('The total number of nodes to process at one time. Set to 0 for all nodes.'),
    '#size' => 4,
    '#default_value' => $batch_size,
  );
  $form['batch_fieldset']['queue_all_nodes'] = array(
    '#type' => 'submit',
    '#value' => t('Queue all nodes for processing'),
    '#submit' => array('eagle_i_integration_queue_all_nodes_callback'),
    '#prefix' => '<div id="queue-all-nodes-button">',
  );
  $form['batch_fieldset']['queued_nodes'] = array(
    '#type' => 'markup',
    '#value' => '(' . $controller->getQueueSize() . ') nodes queued',
    '#suffix' => '</div>',
  );
  $form['batch_fieldset']['clear_queue'] = array(
    '#type' => 'submit',
    '#value' => t('Clear queue'),
    '#submit' => array('eagle_i_integration_clear_queue_callback'),
    '#prefix' => '<div id="clear-nodes-button">',
    '#suffix' => '</div>',
  );
  $form['batch_fieldset']['process_nodes'] = array(
    '#type' => 'submit',
    '#value' => t('Process queued nodes'),
    '#submit' => array('eagle_i_integration_process_queued_nodes_callback'),
    '#prefix' => '<div id="process-nodes-button">',
    '#suffix' => '</div>',
  );

  // SUBMIT.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  // Testing and Config links.
  $form['eagle_i_config_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i">Eagle-I Configuration Page</a>',
    '#prefix' => '<div id="eagle-i-testing-links">',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['connector_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/eagle-i/test/data_connector">DataConnector Test Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['mapper_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/eagle-i/test/data_mapper">DataMapper Test Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['uri_map_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/uri-map">Manually Map Node IDs to URIs</a>',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Main submit handler for the Data Mapping form.
 * @param $form
 * @param $form_state
 */
function eagle_i_integration_map_data_form_submit($form, &$form_state) {

  // Ensure we don't process the whole form from our AHAH callback.
  if (!empty($form_state['ahah_submission'])) return;

  // Retrieve the selected Content Types.
  $mapped_content_types = array();
  $content_types = $form_state['values']['content_types'];
  while (count($content_types) > 0) {
    $type = array_shift($content_types);
    if ($type) {
      $mapped_content_types[] = $type;
    }
  }
  asort($mapped_content_types);

  // Comment out this line in order to disable field creation.
  eagle_i_integration_add_remove_eagle_i_field($mapped_content_types);

  // The the Drupal variables.
  variable_set('eagle_i_source_name', $form_state['values']['source_name']);
  variable_set('eagle_i_enable_node_save', $form_state['values']['enable_node_save_radio']);
  variable_set('mapped_content_types', serialize($mapped_content_types));
  variable_set('eagle_i_batch_size', $form_state['values']['batch_size']);
}

/**
 * Submit handler for the Queue All Nodes button.
 * @param $form
 * @param $form_state
 */
function eagle_i_integration_queue_all_nodes_callback($form, &$form_state) {
  $controller = EIController::getInstance();

  // Load the current mapped content types.
  $content_types = "'" . implode("','", unserialize(variable_get('mapped_content_types', ''))) . "'";

  // Query the db for all nodes of any mapped content type that are explicity allowed to be exported.
  $result = db_query(
    "SELECT DISTINCT n.nid
    FROM node AS n
    INNER JOIN content_field_eaglei_export as ei
    ON n.nid = ei.nid
    WHERE n.type IN ($content_types)
    AND n.status = 1
    AND ei.field_eaglei_export_value = 'TRUE'
    ORDER BY n.nid"
  );
  $nids = array();
  while ($row = db_fetch_array($result)) {
    $nids[] = $row['nid'];
  }

  // Queue the nids.
  $controller->queueNodes($nids);

  // Submit the form.
  eagle_i_integration_map_data_form_submit($form, $form_state);
}

/**
 * Submit handler for the Clear Queue button.
 * @param $form
 * @param $form_state
 */
function eagle_i_integration_clear_queue_callback($form, &$form_state) {
  $controller = EIController::getInstance();

  // Clear the Queue.
  $controller->clearQueue();

  // Submit the form.
  eagle_i_integration_map_data_form_submit($form, $form_state);
}

/**
 * Submit handler for the Process Queued Nodes button.
 * @param $form
 * @param $form_state
 */
function eagle_i_integration_process_queued_nodes_callback($form, &$form_state) {
  $controller = EIController::getInstance();

  // Process nodes (as per entered batch size).
  //$controller->processQueue($form_state['values']['batch_size']);

  // We're implementing Batch API processing here in a later
  // iteration. As such, we're going to break a little of our
  // OO encapsulation and we'll add some coupling, but this
  // is really a better UX solution to batch processing, so we'll
  // go ahead with it.
  $size = $form_state['values']['batch_size'];
  $queue = $controller->queueRemoveAll();
  $batch = array(
    'title' => t('Pushing nodes to Eagle-I...'),
    'operations' => array(),
    'file' => drupal_get_path('module', 'eagle_i_integration') . '/includes/batch_methods.inc',
  );

  if ($size) {
    $count = 0;
    while ($count < $size) {
      $nid = array_shift($queue);
      $batch['operations'][] = array(
        'eagle_i_integration_batch_method_process_node',
        array($nid, TRUE),
      );
      $count++;
    }
    $controller->queueAddAll($queue);
  }
  else {
    foreach ($queue as $nid) {
      $batch['operations'][] = array(
        'eagle_i_integration_batch_method_process_node',
        array($nid),
      );
    }
  }
  batch_set($batch);

  // Save the form.
  eagle_i_integration_map_data_form_submit($form, $form_state);
}

/**
 * Submit handler for the Update Site Ontology button.
 * @param $form
 * @param $form_state
 */
function eagle_i_integration_reset_site_ontology_callback($form, &$form_state) {
  $controller = EIController::getInstance();

  // Update the Site Ontology with the EI Node Ontology.
  variable_set('ontology_version', $controller->getOntologyVersion());
}

/**
 * Helper function to retrieve current errors.
 * @return array
 */
function eagle_i_integration_get_current_errors() {
  $controller = EIController::getInstance();

  // Get the current Queue
  $queue = $controller->getQueue();

  // Prepare an error instance for any error for any queued nid.
  $errors = array();
  if ($queue) {
    foreach ($queue as $nid) {
      $error = $controller->getError($nid);
      if ($error) {
        $errors[$nid] = array();
        $node = node_load($nid, NULL, TRUE);
        $errors[$nid]['title'] = $node->title;
        $errors[$nid]['path'] = $node->path;
        $errors[$nid]['message'] = $controller->getError($nid);
      }
    }
  }
  return $errors;
}

/**
 * Add and/or remove the eagle-i export field from content types
 * @param $content_types
 */
function eagle_i_integration_add_remove_eagle_i_field($content_types) {
  // Include the necessary CCK module CRUD file
  module_load_include('inc', 'content', 'includes/content.crud');

  // If this field hasn't yet been created
  if (!db_table_exists('content_field_eaglei_export')) {
    $eagle_i_export_field = array (
      'field_name' => 'field_eaglei_export',
      'type_name' => '',
      'display_settings' => array (
        'label' => array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => array (
          'format' => 'default',
          'exclude' => 0,
        ),
        2 => array (
          'format' => 'default',
          'exclude' => 0,
        ),
        3 => array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'cck_blocks' => array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
      'widget_active' => '1',
      'type' => 'text',
      'required' => '0',
      'multiple' => '0',
      'db_storage' => '1',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'columns' => array (
        'value' => array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => "FALSE\nTRUE|Export to Eagle-I?",
      'allowed_values_php' => '',
      'widget' => array (
        'default_value' => array (
          0 => array (
            'value' => 'TRUE',
          ),
        ),
        'default_value_php' => TRUE,
        'label' => 'Export to Eagle-I?',
        'weight' => '-2',
        'description' => '',
        'type' => 'optionwidgets_onoff',
        'module' => 'optionwidgets',
      ),
    );
  }

  // Otherwise, get an instance of the field and set it's default to true
  else {
    $eagle_i_export_field = content_fields('field_eaglei_export');
    $eagle_i_export_field['widget']['default_value'] = array (
      0 => array (
        'value' => 'TRUE',
      ),
    );
  }

  // Boolean for cache clearing
  $menu_changed = FALSE;

  // Grab the mapped content types array - these are the previously mapped types as
  // we haven't rewritten the field at this point - and compare them with the form state.
  $previously_mapped_content_types = unserialize(variable_get('mapped_content_types', null));
  $remove_from_content_types = null;
  if (!$content_types && $previously_mapped_content_types) {
    $remove_from_content_types = $previously_mapped_content_types;
  }
  if ($previously_mapped_content_types && $content_types) {
    $remove_from_content_types = array_diff($previously_mapped_content_types, $content_types);
  }

  // Remove the eaglei_export field from any content types we're no longer mapping,
  if ($remove_from_content_types) {
    foreach($remove_from_content_types as $content_type) {
      content_field_instance_delete('field_eaglei_export', $content_type, FALSE);
      $menu_changed = TRUE;
    }
  }

  // Add the eaglei_export field to any content types we're newly mapping.
  if ($content_types) {
    foreach ($content_types as $content_type) {
      // Check if the content type exists, adding if it doesn't.
      if (!content_fields('field_eaglei_export', $content_type)) {
        // Set the content type for the field and create the instance.
        $eagle_i_export_field['type_name'] = $content_type;
        content_field_instance_create($eagle_i_export_field, FALSE);

        // Set the menu changed flag.
        $menu_changed = TRUE;
      }
    }

    // Clear caches and rebuild the menu if the menu has changed.
    if ($menu_changed) {
      content_clear_type_cache(TRUE);
      menu_rebuild();
    }

    foreach ($content_types as $content_type) {
      // We'll need to update the value with the default for any nodes of
      // the content type that already exist; collect the nids for the content type.
      $nids = array();
      $result = db_query("
          SELECT nid, vid
          FROM node
          WHERE type = '%s'
          AND status = 1
          ORDER BY nid
        ", $content_type);
      while ($row = db_fetch_array($result)) {
        $nids[] = array('nid' => $row['nid'], 'vid' => $row['vid']);
      }

      // Loop through the nids and update the db with the default field value.
      foreach ($nids as $nid) {
        $data = array(
          'vid' => $nid['vid'],
          'nid' => $nid['nid'],
          'field_eaglei_export_value' => 'TRUE',
        );
        @drupal_write_record('content_field_eaglei_export', $data);
      }
    }
  }
}

function _eagle_i_integration_create_type_maps(&$form, $mapped_content_types) {

  if (!isset($form['content_type_mapping_fieldset'])) {
    $form['content_type_mapping_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content Type Mapping'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#prefix' => '<div id="content_type_mapping_fieldset">',
      '#suffix' => '</div>',
    );
  }
  $form['content_type_mapping_fieldset']['message'] = array(
    '#type' => 'markup',
    '#value' => '<div id="message">You will need to edit the content_type_mapping.xml file located in the /xml folder in the base module directory in order to map content types. Refer to the included .xsd file to create the requisite mappings for your needs.</div>',
  );

  ////////////////////////////////////////////////////////////////
  // PENDING DEVELOPMENT                                        //
  ////////////////////////////////////////////////////////////////
  /*foreach ($mapped_content_types as $type) {
    $form['content_type_mapping_fieldset']['content_types'][$type] = array(
      '#type' => 'markup',
      '#value' => '<div><a href="/admin/config/eagle-i/map-data/map-content-type/' . $type . '">' . $type . '</div>',
    );
  }*/
}