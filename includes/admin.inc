<?php

/**
 * Implementation of hook_form.
 * @param $form
 * @return array
 */
function eagle_i_integration_admin_form($form)
{
  $api_server_protocol = variable_get('api_server_protocol', 0);
  $api_server_url = variable_get('api_server_url', '');
  $api_rest_url = variable_get('api_rest_url', '');
  $api_username = variable_get('api_username', '');

  $form = array();
  $form['data_access_tier'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data Access Tier Settings:'),
  );
  $form['data_access_tier']['api_server_protocol'] = array(
    '#type' => 'radios',
    '#title' => t('API Server Protocol'),
    '#default_value' => $api_server_protocol,
    '#options' => array(t('http'), t('https')),
  );
  $form['data_access_tier']['api_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API Server URL'),
    '#description' => t('The URL of the Eagle-i node server without the protocol, i.e. www.google.com'),
    '#size' => 35,
    '#default_value' => $api_server_url,
  );
  $form['data_access_tier']['api_rest_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API REST URL'),
    '#description' => t('The relative URL of the REST service on the API server, i.e. /rest/api'),
    '#size' => 35,
    '#default_value' => $api_rest_url,
  );
  $form['data_access_tier']['api_user'] = array(
    '#type' => 'textfield',
    '#title' => t('API Username'),
    '#description' => t('The username for login to the Eagle-I API.'),
    '#size' => 15,
    '#default_value' => $api_username,
  );
  $form['data_access_tier']['api_pass'] = array(
    '#type' => 'password_confirm',
    '#title' => t('API Password'),
    '#size' => 25,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  $form['map_data_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/map-data">Map Data to Eagle-I</a>',
    '#prefix' => '<div id="eagle-i-testing-links">',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['connector_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/eagle-i/test/data_connector">DataConnector Test Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['mapper_testing_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/eagle-i/test/data_mapper">DataMapper Test Page</a>',
    '#suffix' => '&nbsp;|&nbsp;',
  );
  $form['uri_map_link'] = array(
    '#type' => 'markup',
    '#value' => '<a href="/admin/config/eagle-i/uri-map">Manually Map Node IDs to URIs</a>',
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Custom submit handler for the eagle_i_integration_admin_form.
 * @param $form
 * @param $form_state
 */
function eagle_i_integration_admin_form_submit($form, &$form_state)
{
  variable_set('api_server_protocol', $form_state['values']['api_server_protocol']);
  variable_set('api_server_url', $form_state['values']['api_server_url']);
  variable_set('api_rest_url', $form_state['values']['api_rest_url']);
  variable_set('api_username', $form_state['values']['api_user']);

  // Hash and salt the password.
  // Since we need access to the password in plain text format, there's
  // really no truly secure way to do this. We'll salt it and encode it,
  // storing the salt and the encoded string in the variables table, so
  // at the very least it will be secure there. We can't hash it, however
  // since we need to return to it's simple text in order to pass it to
  // the Eagle-I simply http authentication. Until Eagle-I implements an
  // Oath for the API, this is about the best we can do.
  $DataConnector = EIDataConnector::getInstance();
  $array = $DataConnector->makePass($form_state['values']['api_pass']);

  variable_set('api_password', $array['pass']);
  variable_set('offset', $array['offset']);
  variable_set('interval', $array['int']);
}