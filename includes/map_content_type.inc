<?php

////////////////////////////////////////////////////////////////
// PENDING DEVELOPMENT                                        //
////////////////////////////////////////////////////////////////

function eagle_i_integration_map_content_type_form($form) {
  drupal_add_js(drupal_get_path('module', 'eagle_i_integration') . '/scripts/map_content_type.js');

  $form = array();

  $type = arg(5);
  if (!$type) {
    drupal_set_message("No content type selected. Please use the provided UI.");
    return $form;
  }

  $DataMapper = EIDataMapper::getInstance();
  $fields = array_shift(_eagle_i_integration_get_fields_for_mapped_types(array($type)));
  $map = $DataMapper->getMap($type);

  $form['content_type_mapping'] = array(
    '#type' => 'fieldset',
    '#title' => t($type),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="' . $type . '-mapping">',
    '#suffix' => '</div>',
  );

  $form['content_type_mapping']['specific-type-uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Specific Type URI'),
    '#default_value' => ($map) ? (string) $map->specific_type_uri : '',
  );
  $form['content_type_mapping']['root-type-uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Root Type URI'),
    '#default_value' => ($map) ? (string) $map->root_type_uri : '',
  );
  $form['content_type_mapping']['title-field'] = array(
    '#type' => 'select',
    '#title' => t('Title Field'),
    '#options' => $fields,
    '#default_value' => ($map) ? (string) $map->title_field : '',
  );

  $default_path_uri = '';
  if ($map && $map->text_properties_mapping->text_property->field == 'path') {
    $default_path_uri = (string) $map->text_properties_mapping->text_property->uri;
    unset($map->text_properties_mapping);
  } else if ($map) {
    $i = 0;
    foreach ($map->text_properties_mapping->text_property as $property) {
      if ($property->field == 'path') {
        $default_path_uri = (string) $property->uri;
        unset($map->text_properties_mapping->text_property[$i]);
        break;
      }
      $i++;
    }
  }
  $form['content_type_mapping']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $default_path_uri,
  );

  $form['content_type_mapping']['table-wrapper'] = array(
    '#type' => 'markup',
    '#value' => '<div id="table-wrapper-wrapper">',
  );

  $form['content_type_mapping']['table'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="' . $type . '-table-wrapper"><table id="' . $type . '">
          <thead>
            <tr>
              <th class="mapping-table-field-column">Field</th>
              <th class="mapping-table-spacer-column"></th>
              <th class="mapping-table-uri-column">Eagle-I URI</th>
            </tr>
          </thead>
          <tbody>',
    '#suffix' => '</tbody></table></div>',
  );

  $i = 0;
  if ($map) {
    if ($map->text_properties_mapping) {
      foreach ($map->text_properties_mapping->text_property as $mapping) {
        $form['content_type_mapping']['table']['field-' . $i] = array(
          '#type' => 'select',
          '#options' => $fields,
          '#prefix' => '<tr><td class="mapping-table-field-column">',
          '#suffix' => '</td><td class="mapping-table-spacer-column">&harr;</td></td>',
          '#default_value' => (string) $mapping->field,
          '#attributes' => array(
            'class' => 'field-select',
          ),
        );
        $form['content_type_mapping']['table']['uri-' . $i] = array(
          '#type' => 'textfield',
          '#prefix' => '<td class="mapping-table-uri-column">',
          '#suffix' => '</td></tr>',
          '#default_value' => (string) $mapping->uri,
        );

        $attributes = count($mapping->attributes());
        if ($attributes) {
          $attributes = array();
          foreach ($mapping->attributes() as $k => $attribute) {
            $attributes[$k] = (string) $attribute;
          }
        }
        $form['content_type_mapping']['table']['controls-' . $i]['prepend-' . $i] = array(
          '#type' => 'checkbox',
          '#title' => t('Prepend'),
          '#default_value' => ($attributes) ? array_key_exists('prepend', $attributes) : 0,
          '#prefix' => '<tr><td colspan="3">',
          '#attributes' => array(
            'class' => 'prepend-control',
          ),
        );
        $form['content_type_mapping']['table']['controls-' . $i]['append-' . $i] = array(
          '#type' => 'checkbox',
          '#title' => t('Append'),
          '#default_value' => ($attributes) ? array_key_exists('append', $attributes) : 0,
          '#attributes' => array(
            'class' => 'append-control',
          ),
        );
        $form['content_type_mapping']['table']['controls-' . $i]['prepend-text-' . $i] = array(
          '#type' => 'textfield',
          '#title' => t('Prepend with'),
          '#default_value' => ($attributes) ? array_key_exists('prepend', $attributes) ? $attributes['prepend'] : '' : '',
          '#attributes' => array(
            'class' => 'prepend-control',
          ),
        );
        $form['content_type_mapping']['table']['controls-' . $i]['append-text-' . $i] = array(
          '#type' => 'textfield',
          '#title' => t('Append with'),
          '#default_value' => ($attributes) ? array_key_exists('append', $attributes) ? $attributes['append'] : '' : '',
          '#suffix' => '</td></tr>',
          '#attributes' => array(
            'class' => 'append-control',
          ),
        );
        $i++;
      }
    }

    if ($map->linked_resources_mapping) {
      foreach ($map->linked_resources_mapping->linked_resource as $resource) {
        $reverse = count($resource->attributes());
        $reverse_type = null;
        if ($reverse) {
          foreach ($resource->attributes() as $k => $v) {
            if ($k == 'reverse') $reverse_type = (string) $v;
          }
        }
        $form['content_type_mapping']['table']['field-' . $i] = array(
          '#type' => 'select',
          '#options' => $fields,
          '#prefix' => '<tr><td class="mapping-table-field-column">',
          '#suffix' => '</td><td class="mapping-table-spacer-column">&harr;</td></td>',
          '#default_value' => ($reverse)
            ? 'reverse-' . $reverse_type . '-' . (string) $resource->field
            : 'reference-' . (string) $resource->field,
          '#attributes' => array(
            'class' => 'field-select',
          ),
        );
        $form['content_type_mapping']['table']['uri-' . $i] = array(
          '#type' => 'textfield',
          '#prefix' => '<td class="mapping-table-uri-column">',
          '#suffix' => '</td></tr>',
          '#default_value' => (string) $resource->uri,
        );
        $form['content_type_mapping']['table']['controls-' . $i]['prepend-' . $i] = array(
          '#type' => 'checkbox',
          '#title' => t('Prepend'),
          '#default_value' => 0,
          '#prefix' => '<tr><td colspan="3">',
          '#attributes' => array(
            'class' => 'prepend-control',
          ),
        );
        $form['content_type_mapping']['table']['controls-' . $i]['append-' . $i] = array(
          '#type' => 'checkbox',
          '#title' => t('Append'),
          '#default_value' => 0,
          '#attributes' => array(
            'class' => 'append-control',
          ),
        );
        $form['content_type_mapping']['table']['controls-' . $i]['prepend-text-' . $i] = array(
          '#type' => 'textfield',
          '#title' => t('Prepend with'),
          '#default_value' => '',
          '#attributes' => array(
            'class' => 'prepend-control',
          ),
        );
        $form['content_type_mapping']['table']['controls-' . $i]['append-text-' . $i] = array(
          '#type' => 'textfield',
          '#title' => t('Append with'),
          '#default_value' => '',
          '#suffix' => '</td></tr>',
          '#attributes' => array(
            'class' => 'append-control',
          ),
        );
        $i++;
      }
    }
  }

  if (!$map || (!$map->text_properties_mapping && !$map->linked_resources_mapping)) {
    $form['content_type_mapping']['table']['field-' . $i] = array(
      '#type' => 'select',
      '#options' => $fields,
      '#prefix' => '<tr><td class="mapping-table-field-column">',
      '#suffix' => '</td><td class="mapping-table-spacer-column">&harr;</td></td>',
      '#default_value' => '',
    );
    $form['content_type_mapping']['table']['uri-' . $i] = array(
      '#type' => 'textfield',
      '#prefix' => '<td class="mapping-table-uri-column">',
      '#suffix' => '</td></tr>',
      '#default_value' => '',
    );
    $form['content_type_mapping']['table']['controls-' . $i]['prepend-' . $i] = array(
      '#type' => 'checkbox',
      '#title' => t('Prepend'),
      '#default_value' => 0,
      '#prefix' => '<tr><td colspan="3">',
      '#attributes' => array(
        'class' => 'prepend-control',
      ),
    );
    $form['content_type_mapping']['table']['controls-' . $i]['append-' . $i] = array(
      '#type' => 'checkbox',
      '#title' => t('Append'),
      '#default_value' => 0,
      '#attributes' => array(
        'class' => 'append-control',
      ),
    );
    $form['content_type_mapping']['table']['controls-' . $i]['prepend-text-' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Prepend with'),
      '#default_value' => '',
      '#attributes' => array(
        'class' => 'prepend-control',
      ),
    );
    $form['content_type_mapping']['table']['controls-' . $i]['append-text-' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Append with'),
      '#default_value' => '',
      '#suffix' => '</td></tr>',
      '#attributes' => array(
        'class' => 'append-control',
      ),
    );
  }
  $form['content_type_mapping']['close-wrapper'] = array(
    '#type' => 'markup',
    '#value' => '</div>',
  );

  $form['content_type_mapping']['add-mapping'] = array(
    '#type' => 'submit',
    '#value' => 'Add Mapping',
    '#ahah' => array(
      'path' => 'admin/config/eagle-i/add-field-mapping',
      'wrapper' => 'table-wrapper-wrapper',
      'effect' => 'fade',
    ),
  );

  return $form;
}

function _eagle_i_integration_get_fields_for_mapped_types($mapped_content_types) {

  // Set up the main fields array & retrieve the content types.
  $fields = array();
  $types = node_get_types('names');

  // Get all nodereference CCK fields so we can parse the reverse references.
  $all_fields = content_fields();
  foreach ($all_fields as $field_name => $field) {
    if ($field['type'] != 'nodereference') unset($all_fields[$field_name]);
  }

  // Set up the node properties to map.
  $node_properties = array(
    'title' => 'title',
    'body' => 'body',
    'teaser' => 'teaser',
    'status' => 'status',
    'created' => 'created',
  );

  // Loop through all content types being mapped.
  foreach ($mapped_content_types as $type) {

    // Set up the fields array.
    $fields[$type] = array();

    // Add the node properties we'd like to be able to map.
    foreach ($node_properties as $key => $property) {
      $fields[$type][$key] = $property;
    }

    // Query the db for the CCK fields on this content type.
    $result = @db_query("
      SELECT field_name
      FROM {content_node_field_instance}
      WHERE type_name = '%s'
    ", $type);

    // Add the CCK fields to the fields array.
    while ($row = db_fetch_array($result)) {
      $this_field = content_fields($row['field_name'], $type);
      $machine_name = ($this_field['type'] == 'nodereference') ? 'reference-' . $this_field['field_name'] : $this_field['field_name'];
      $human_name = ($this_field['type'] == 'nodereference') ? $this_field['field_name'] . ' (reference)' : $this_field['field_name'];
      $fields[$type][$machine_name] = $human_name;
    }

    // Add all the reverse references for this content type to the fields array.
    $ref_fields = array();
    foreach ($all_fields as $field_name => $field) {
      foreach ($types as $machine_name => $human_name) {
        $this_field = content_fields($field_name, $machine_name);
        if ($this_field) {
          $ref_fields['reverse-' . $machine_name . '-' . $field_name]
            = $field_name . ' (reverse reference on ' . $human_name . ')';
        }
      }
    }
    $fields[$type] = array_merge($fields[$type], $ref_fields);
  }

  // Return the fields array.
  return $fields;
}

function eagle_i_integration_add_field_mapping_callback() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE, 'rebuild' => TRUE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form_state['ahah_submission'] = TRUE;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);

  $content_type_mapping = $form['content_type_mapping']['table'];

  $type = $form['content_type_mapping']['#title'];
  $fields = array_shift(_eagle_i_integration_get_fields_for_mapped_types(array($type)));

  $additional_mappings = 0;
  if (isset($form_state['storage']['additional-mappings'])) {
    $additional_mappings = $form_state['storage']['quantity'];
  }
  if (isset($form_state['values']['add-mapping']) && $form_state['values']['op'] == "Add Mapping") {
    $additional_mappings++;
  }

  $form['additional_mappings'] = array(
    '#type' => 'value',
    '#value' => $additional_mappings,
  );

  for ($i = 0; $i < $additional_mappings; $i++) {
    $content_type_mapping['additional_mapping']['field-' . $i] = array(
      '#type' => 'select',
      '#options' => $fields,
      '#prefix' => '<tr><td class="mapping-table-field-column">',
      '#suffix' => '</td><td class="mapping-table-spacer-column">&harr;</td></td>',
      '#default_value' => '',
    );
    $content_type_mapping['additional_mapping']['uri-' . $i] = array(
      '#type' => 'textfield',
      '#prefix' => '<td class="mapping-table-uri-column">',
      '#suffix' => '</td></tr>',
      '#default_value' => '',
    );
    $content_type_mapping['additional_mapping']['controls-' . $i]['prepend-' . $i] = array(
      '#type' => 'checkbox',
      '#title' => t('Prepend'),
      '#default_value' => 0,
      '#prefix' => '<tr><td colspan="3">',
      '#attributes' => array(
        'class' => 'prepend-control',
      ),
    );
    $content_type_mapping['additional_mapping']['controls-' . $i]['append-' . $i] = array(
      '#type' => 'checkbox',
      '#title' => t('Append'),
      '#default_value' => 0,
      '#attributes' => array(
        'class' => 'append-control',
      ),
    );
    $content_type_mapping['additional_mapping']['controls-' . $i]['prepend-text-' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Prepend with'),
      '#default_value' => '',
      '#attributes' => array(
        'class' => 'prepend-control',
      ),
    );
    $content_type_mapping['additional_mapping']['controls-' . $i]['append-text-' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Append with'),
      '#default_value' => '',
      '#suffix' => '</td></tr>',
      '#attributes' => array(
        'class' => 'append-control',
      ),
    );
  }


  $output = drupal_render($content_type_mapping);

  /*// the following code taken from http://api.drupalize.me/api/drupal/function/filefield_js/6
  // Loop through the JS settings and find the settings needed for our buttons.
  $javascript = drupal_add_js(NULL, NULL);
  $anothermapping_ahah_settings = array();
  if (isset($javascript['setting'])) {
    foreach ($javascript['setting'] as $settings) {
      if (isset($settings['ahah'])) {
        foreach ($settings['ahah'] as $id => $ahah_settings) {
          if (strpos($id, 'add-mapping')) {
            $anothermapping_ahah_settings[$id] = $ahah_settings;
          }
        }
      }
    }
  }
  // Add the AHAH settings needed for our new buttons.
  if (!empty($anothermapping_ahah_settings)) {
    $output .= '<script type="text/javascript">jQuery.extend(Drupal.settings.ahah, ' . drupal_to_js($anothermapping_ahah_settings) . ');</script>';
  }
  // end of drupalize.me code*/

  $output = theme('status_messages').$output;

  // Final rendering callback.
  drupal_json(array('status' => TRUE, 'data' => $output));

  exit();
}

function theme_eagle_i_integration_map_content_type_form($form) {
  $path = drupal_get_path('module', 'eagle_i_integration');
  drupal_add_js($path . '/scripts/eagle_i_integration_map_content_type_ahah_callback.js');
  return drupal_render($form);
}